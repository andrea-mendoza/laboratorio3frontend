import { Genre } from './genre';

export class Book{
    public id:number
    constructor(
        public title:string,
        public author:string,
        public description:string,
        public status:string,
        public language:string,
        public genre:Genre,
        public publishedDate:string
    ){}
}