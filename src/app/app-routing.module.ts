import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ShowBooksComponent } from './show-books/show-books.component';
import { CreateBookComponent } from './create-book/create-book.component';
import { EditBookComponent } from './edit-book/edit-book.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'listado',component:ShowBooksComponent},
  {path:'crear',component:CreateBookComponent},
  {path:'editarLibro/:id',component:EditBookComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
