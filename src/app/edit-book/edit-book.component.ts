import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Book } from '../models/book';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {
  public book;
  public title = "Editar Libro";
  public listStatus:any = [];
  public listGenre;
  public actual;
  public date;

  constructor(private httpClient: HttpClient,private route: ActivatedRoute,private router: Router) { 
    this.route.paramMap.subscribe(params => {
      this.getBook(this.route.snapshot.params.id);
    });

    this.listStatus[0] = "Publicado";
    this.listStatus[1] = "En Edicion";
    this.listStatus[2] = "No Publicado";

    this.httpClient.get('http://localhost:8081/allGenres').subscribe((res : any[])=>{
        this.listGenre = res;
    });

    this.date = new FormControl(new Date());
  }

  ngOnInit() {
    this.book = new Book("","","","","",{id:0,name:""},""); 
  }

  getBook(id){
    this.httpClient.get('http://localhost:8081/getById/'+id).subscribe((res : any[])=>{
      this.book = res;
      this.book.publishedDate = new Date(this.book.publishedDate);
    });
  }

  onSubmit(){
    let formatDay = "";

    var today = new Date(this.book.publishedDate);
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //because January is 0! 
    var yyyy = today.getFullYear();

    formatDay = dd+ "-"+mm+"-"+yyyy;
    this.book.publishedDate = formatDay;
    console.log(this.book.publishedDate);

    this.route.paramMap.subscribe(params => {
      this.httpClient.put(`http://localhost:8081/editBook/`+this.route.snapshot.params.id,this.book)
      .subscribe(
        (data:any) => {
          this.router.navigateByUrl('/');
        }
      )
    });
    
  }

}
