import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-show-books',
  templateUrl: './show-books.component.html',
  styleUrls: ['./show-books.component.css']
})
export class ShowBooksComponent implements OnInit {
  displayedColumns: string[] = ['title', 'author', 'description', 'status', 'language', 'publishedDate', 'genre','options'];
  public dataSource;

  constructor(private httpClient: HttpClient) {
  }

  ngOnInit() {
    this.httpClient.get('http://localhost:8081/allBooks').subscribe((res : any[])=>{
        console.log(res);
        this.dataSource = res;
    });
  }

  deleteBook(id){
    this.httpClient.delete('http://localhost:8081/deleteBook/'+id).subscribe((data)=>{
      window.location.reload();
    });
  }

}
