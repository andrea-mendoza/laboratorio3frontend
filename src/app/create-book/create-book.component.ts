import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css']
})
export class CreateBookComponent implements OnInit {
  public title = "Crear Un libro";
  public book: Book;
  public listStatus:any = [];
  public listGenre;
      
  constructor(private http: HttpClient) {
    this.book = new Book("","","","","",null,"");
   }

  ngOnInit() {
    this.listStatus[0] = "Publicado";
    this.listStatus[1] = "En Edicion";
    this.listStatus[2] = "No Publicado";

    this.http.get('http://localhost:8081/allGenres').subscribe((res : any[])=>{
        this.listGenre = res;
    });

  }

  onSubmit(){
    let formatDay = "";

    var today = new Date(this.book.publishedDate);
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //because January is 0! 
    var yyyy = today.getFullYear();

    formatDay = dd+ "-"+mm+"-"+yyyy;
    this.book.publishedDate = formatDay;
    console.log(this.book.publishedDate);
    this.http.post(`http://localhost:8081/newBook`,this.book)
    .subscribe(
      (data:any) => {
        window.location.reload();
      }
    )
  }

}
